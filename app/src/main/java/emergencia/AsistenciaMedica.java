package emergencia;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import emergencia.ResAsisMedica.TipoAsistencia;

public class AsistenciaMedica {
    private Estado estado;
    private TipoSolicitante tipo_solicitante;
    private LocalDate ultima_fecha_pago;
    private ResAsisMedica resultado;

    public enum TipoSolicitante {
        Afiliado,
        Familiar
    }

    public enum Estado{
        En_curso,
        Finalizado,
        Cancelado
    }


    public AsistenciaMedica() {
    }

    public AsistenciaMedica(TipoSolicitante tipo_solicitante, LocalDate ultima_fecha_pago, Estado estado,
                            TipoAsistencia tipo_asistencia, String diagnostico) {
        this.tipo_solicitante = tipo_solicitante;
        this.ultima_fecha_pago = ultima_fecha_pago;
        this.estado = estado;
        this.resultado = new ResAsisMedica(tipo_asistencia, diagnostico);
    }

    public TipoSolicitante getTipo_solicitante() {
        return tipo_solicitante;
    }

    public void setTipo_solicitante(TipoSolicitante tipo_solicitante) {
        this.tipo_solicitante = tipo_solicitante;
    }

    public LocalDate getUltima_fecha_pago() {
        return ultima_fecha_pago;
    }

    public void setUltima_fecha_pago(LocalDate ultima_fecha_pago) {
        this.ultima_fecha_pago = ultima_fecha_pago;
    }


    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String verificarPago() {
        LocalDate today = LocalDate.now();
        long monthsBetween = ChronoUnit.MONTHS.between(ultima_fecha_pago, today);
        boolean estado = monthsBetween <= 2;
        if(estado == true){
            return("\n\nEl afiliado se encuentra dentro del plazo de mora.");
        }
        else{
            return("\n\n¡El afiliado se encuentra FUERA del plazo!");
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n---Asistencia Medica---");
        sb.append("\nTipo de solicitante: ").append(tipo_solicitante);
        sb.append("\nUltima fecha de pago: ").append(ultima_fecha_pago);
        sb.append("\nEstado: ").append(estado);
        sb.append(resultado);
        sb.append(verificarPago());
        return sb.toString();
    }
}