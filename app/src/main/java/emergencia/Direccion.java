package emergencia;

public class Direccion{
    public Provincia provincia;
    public String localidad;
    public String barrio;
    public String calle;
    public Integer numero_calle;


    public enum Provincia{
        Buenos_Aires,
        Ciudad_Autonoma_de_Buenos_Aires,
        Catamarca,
        Chaco,
        Chubut,
        Cordoba,
        Corrientes,
        Entre_Rios,
        Formosa,
        Jujuy,
        La_Pampa,
        La_Rioja,
        Mendoza,
        Misiones,
        Neuquen,
        Rio_Negro,
        Salta,
        San_Juan,
        San_Luis,
        Santa_Cruz,
        Santa_Fe,
        Santiago_del_Estero,
        Tierra_del_Fuego,
        Tucuman
}


    public Direccion(){
    }
    public Direccion(Provincia provincia, String localidad, String barrio, String calle, Integer numero_calle){
        this.provincia = provincia;
        this.localidad = localidad;
        this.barrio = barrio;
        this.calle  = calle;
        this.numero_calle = numero_calle;
    }
    public Provincia getProvincia() {
        return provincia;
    }
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
    public String getLocalidad() {
        return localidad;
    }
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
    public String getBarrio() {
        return barrio;
    }
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }
    public String getCalle() {
        return calle;
    }
    public void setCalle(String calle) {
        this.calle = calle;
    }
    public Integer getNumero_calle() {
        return numero_calle;
    }
    public void setNumero_calle(Integer numero_calle) {
        this.numero_calle = numero_calle;
    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nProvincia: ").append(provincia);
        sb.append("\nLocalidad: ").append(localidad);
        sb.append("\nBarrio: ").append(barrio);
        sb.append("\nCalle: ").append(calle);
        sb.append("\nNumero de calle: ").append(numero_calle);

        return sb.toString();
    }
}
