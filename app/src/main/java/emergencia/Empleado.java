package emergencia;

import java.time.LocalDate;
import java.time.LocalTime;

public class Empleado {
    public Puesto puesto;
    public LocalTime horario_entrada;
    public LocalTime horario_salida;
    public double sueldo;
    public String cbu;
    public LocalDate fecha_ingreso;


    public enum Puesto{
        Administrativo,
        Doctor,
        Enfermero,
        Chofer
    }
    public Empleado(
                Puesto puesto, LocalDate fecha_ingreso, LocalTime horario_entrada, LocalTime horario_salida, 
                double sueldo, String cbu){
        
        this.puesto = puesto;
        this.horario_entrada = horario_entrada;
        this.horario_salida = horario_salida;
        this.sueldo = sueldo;
        this.cbu = cbu;
        this.fecha_ingreso = fecha_ingreso;
    }
    public Puesto getPuesto() {
        return puesto;
    }
    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }
    public LocalTime getHorario_entrada() {
        return this.horario_entrada;
    }
    public void setHorario_entrada(LocalTime horario_entrada) {
        this.horario_entrada = horario_entrada;
    }
    public LocalTime getHorario_salida() {
        return this.horario_salida;
    }
    public void setHorario_salida(LocalTime horario_salida) {
        this.horario_salida = horario_salida;
    }
    public double getSueldo() {
        return this.sueldo;
    }
    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }
    public String getCbu() {
        return this.cbu;
    }
    public void setCbu(String cbu) {
        this.cbu = cbu;
    }
    public LocalDate getFecha_ingreso() {
        return this.fecha_ingreso;
    }
    public void setFecha_ingreso(LocalDate fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Empleado--");
        sb.append("\nPuesto: ").append(puesto);
        sb.append("\nHorario entrada: ").append(horario_entrada);
        sb.append("\nHorario salida: ").append(horario_salida);
        sb.append("\nSueldo: ").append(sueldo);
        sb.append("\nCBU: ").append(cbu);
        sb.append("\nFecha ingreso a la empresa: ").append(fecha_ingreso);
        sb.append("\n------------");
        return sb.toString();
    }

}
