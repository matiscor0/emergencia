package emergencia;


public class Movil {
    private Marca marca;
    private int modelo;
    private String patente;
    private TipoMovil tipo_movil;
    private String ubicacion;
    private Disponibilidad_movil disponibilidad;

    public enum Marca{
        Renault,
        Fiat,
        Peugeot,
        Volkswagen,
        Ford,
        Citroen,
        Iveco,
        Toyota,
        Chevrolet
    }
    public enum Disponibilidad_movil{
        Disponible,
        Ocupado
    }
    public Movil() {}

    public Movil(TipoMovil.Tipo tipo_vehiculo,String descripcion,
                Marca marca, int modelo, String patente,
                String ubicacion, Disponibilidad_movil disponibilidad){
                this.tipo_movil = new TipoMovil(tipo_vehiculo, descripcion);
                this.marca = marca;
                this.modelo = modelo;
                this.patente = patente;
                this.ubicacion = ubicacion;
                this.disponibilidad = disponibilidad;
    }
    public Marca getMarca() {
        return this.marca;
    }
    public void setMarca(Marca marca) {
        this.marca = marca;
    }
    public int getModelo() {
        return this.modelo;
    }
    public void setModelo(int modelo) {
        this.modelo = modelo;
    }
    public String getPatente() {
        return this.patente;
    }
    public void setPatente(String patente) {
        this.patente = patente;
    }
    public TipoMovil getTipo_movil() {
        return this.tipo_movil;
    }
    public void setTipo_movil(TipoMovil tipo_movil) {
        this.tipo_movil = tipo_movil;
    }
    public String getUbicacion() {
        return this.ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    public Disponibilidad_movil getDisponibilidad() {
        return this.disponibilidad;
    }
    public void setDisponibilidad(Disponibilidad_movil disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Movil---");
        sb.append(tipo_movil);
        sb.append("\nMarca: ").append(marca);
        sb.append("\nModelo: ").append(modelo);
        sb.append("\nPatente: ").append(patente);
        sb.append("\nUbicacion del movil: ").append(ubicacion);
        sb.append("\nDisponibilidad del movil: ").append(disponibilidad);
        sb.append("\n***Movil***\n");
        return sb.toString();
    }

}
