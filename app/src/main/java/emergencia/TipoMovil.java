package emergencia;

public class TipoMovil {
    private Tipo tipo_vehiculo;
    private String descripcion;


    public enum Tipo{
        Ambulancia,
        Emergencia,
        Transporte,
        Administrativo,
        Mantenimiento
    }
    public TipoMovil(){}

    public TipoMovil(Tipo tipo_vehiculo, String descripcion){
        this.tipo_vehiculo = tipo_vehiculo;
        this.descripcion = descripcion;
    }
    public Tipo getTipo_vehiculo() {
        return this.tipo_vehiculo;
    }
    public void setTipo_vehiculo(Tipo tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nTipo de vehiculo: ").append(tipo_vehiculo);
        sb.append("\nDescripcion del vehiculo: ").append(descripcion);
        return sb.toString();
    }
}
