package emergencia;

import java.time.LocalDate;

import emergencia.Direccion.Provincia;

public class GrupoFamiliar extends Persona {
    private String relacion_afiliado;
    private EstadoRelacion estado_relacion;

    public enum EstadoRelacion {
        Activo,
        Inactivo,
        Espera
    }
   
    public GrupoFamiliar(String nombre_completo, LocalDate fecha_nacimiento, Integer dni, Provincia provincia,
            String localidad, String barrio, String calle, Integer nro_calle, Integer nro_telefono, String correo,
            String relacion_afiliado, EstadoRelacion estado_relacion) {
        super(nombre_completo, fecha_nacimiento, dni, provincia, localidad, barrio, calle, nro_calle, nro_telefono,
                correo);
        this.relacion_afiliado = relacion_afiliado;
        this.estado_relacion = estado_relacion;
    }


    public GrupoFamiliar(String relacion_afiliado, EstadoRelacion estado_relacion) {
        this.relacion_afiliado = relacion_afiliado;
        this.estado_relacion = estado_relacion;
    }


    public String getRelacion_afiliado() {
        return relacion_afiliado;
    }


    public void setRelacion_afiliado(String relacion_afiliado) {
        this.relacion_afiliado = relacion_afiliado;
    }


    public EstadoRelacion getEstado_relacion() {
        return estado_relacion;
    }


    public void setEstado_relacion(EstadoRelacion estado_relacion) {
        this.estado_relacion = estado_relacion;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()+ "\nRelacion con afiliado: " + relacion_afiliado + "\nEstado de relacion: " + estado_relacion);
        return sb.toString();
    }
}
