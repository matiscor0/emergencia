package emergencia;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PagoEmpleado extends Pago{
    public PagoEmpleado(LocalDate fecha_pago, BigDecimal monto_pago, Integer nro_pago, MetodoPago metodo_pago,
            String detalle_pago) {
        super(fecha_pago, monto_pago, nro_pago, metodo_pago, detalle_pago);
    }
    @Override
    public String toString() {
        return super.toString();
    }
}
