package emergencia;


public class ResAsisMedica {
    private TipoAsistencia tipo_asistencia;
    private String diagnostico;

    public enum TipoAsistencia {
        Emergencia,
        Urgencia,
        Atencion,
        Hospitalizacion
    }

    public ResAsisMedica(){}

    public ResAsisMedica(TipoAsistencia tipo_asistencia, String diagnostico){
        this.tipo_asistencia = tipo_asistencia;
        this.diagnostico = diagnostico;
    }

    public TipoAsistencia getTipo_asistencia() {
        return tipo_asistencia;
    }
    public void setTipo_asistencia(TipoAsistencia tipo_asistencia) {
        this.tipo_asistencia = tipo_asistencia;
    }
    public String getDiagnostico() {
        return diagnostico;
    }
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n--- Resultado de Asistencia Medica ---");
        sb.append("\nTipo de Asistencia: ").append(tipo_asistencia);
        sb.append("\nDiagnostico medico: ").append(diagnostico);
        sb.append("\n------------");
        return sb.toString();
    }

}
