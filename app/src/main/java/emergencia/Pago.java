package emergencia;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Pago {
    private LocalDate fecha_pago;
    private BigDecimal monto_pago;
    private Integer nro_pago;
    private MetodoPago metodo_pago;
    private String detalle_pago;

    public enum MetodoPago {
        Efectivo,
        Credito,
        Transferencia,
        Debito
    };

    public Pago(LocalDate fecha_pago, BigDecimal monto_pago, Integer nro_pago, MetodoPago metodo_pago, String detalle_pago) {
        this.fecha_pago = fecha_pago;
        this.monto_pago = monto_pago;
        this.nro_pago = nro_pago;
        this.metodo_pago = metodo_pago;
        this.detalle_pago = detalle_pago;
    }

    public LocalDate getFecha_pago() {
        return fecha_pago;
    }

    public void setFecha_pago(LocalDate fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public BigDecimal getMonto_pago() {
        return monto_pago;
    }

    public void setMonto_pago(BigDecimal monto_pago) {
        this.monto_pago = monto_pago;
    }

    public Integer getNro_pago() {
        return nro_pago;
    }

    public void setNro_pago(Integer nro_pago) {
        this.nro_pago = nro_pago;
    }

    public String getDetalle_pago() {
        return detalle_pago;
    }

    public void setDetalle_pago(String detalle_pago) {
        this.detalle_pago = detalle_pago;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Fecha pago: " + fecha_pago +
        "\nMonto pago: " + monto_pago +
        "\nNumero pago: " + nro_pago +
        "\nMetodo de pago: " + metodo_pago +
        "\nDetalle del pago: " + detalle_pago);
        return sb.toString();
    }
}
