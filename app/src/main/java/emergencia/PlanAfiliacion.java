package emergencia;

public class PlanAfiliacion {
    private String nombre_plan;
    private String descripcion;
    private Integer costo_plan;
    private String beneficio;

    public PlanAfiliacion(String nombre_plan, String descripcion, Integer costo_plan, String beneficio) {
        this.nombre_plan = nombre_plan;
        this.descripcion = descripcion;
        this.costo_plan = costo_plan;
        this.beneficio = beneficio;
    }

    public String getNombre_plan() {
        return nombre_plan;
    }
    public void setNombre_plan(String nombre_plan) {
        this.nombre_plan = nombre_plan;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public Integer getCosto_plan() {
        return costo_plan;
    }
    public void setCosto_plan(Integer costo_plan) {
        this.costo_plan = costo_plan;
    }
    public String getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(String beneficio) {
        this.beneficio = beneficio;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nombre del plan: " + nombre_plan +
                "\nDescripcion del plan: " + descripcion +
                "\nCosto del plan: " + costo_plan +
                "\nBeneficio plan: " + beneficio);
        return sb.toString();
    }
}
