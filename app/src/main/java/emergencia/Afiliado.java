package emergencia;

import java.time.LocalDate;

import emergencia.Direccion.Provincia;

public class Afiliado extends Persona {
    private LocalDate fecha_afiliacion;
    private LocalDate fecha_registro;
    private EstadoAfiliado estado_afiliacion;
    private String tipo_afiliacion;

    public enum EstadoAfiliado{
        Activo,
        Inactivo,
    }

    public Afiliado(String nombre_completo, LocalDate fecha_nacimiento, Integer dni, Provincia provincia,
            String localidad, String barrio, String calle, Integer nro_calle, Integer nro_telefono, String correo,
            LocalDate fecha_afiliacion, LocalDate fecha_registro, EstadoAfiliado estado_afiliacion,
            String tipo_afiliacion) {
        super(nombre_completo, fecha_nacimiento, dni, provincia, localidad, barrio, calle, nro_calle, nro_telefono,
                correo);
        this.fecha_afiliacion = fecha_afiliacion;
        this.fecha_registro = fecha_registro;
        this.estado_afiliacion = estado_afiliacion;
        this.tipo_afiliacion = tipo_afiliacion;
    }

    public LocalDate getFecha_afiliacion() {
        return fecha_afiliacion;
    }

    public void setFecha_afiliacion(LocalDate fecha_afiliacion) {
        this.fecha_afiliacion = fecha_afiliacion;
    }

    public LocalDate getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(LocalDate fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public EstadoAfiliado getEstado_afiliacion() {
        return estado_afiliacion;
    }

    public void setEstado_afiliacion(EstadoAfiliado estado_afiliacion) {
        this.estado_afiliacion = estado_afiliacion;
    }

    public String getTipo_afiliacion() {
        return tipo_afiliacion;
    }

    public void setTipo_afiliacion(String tipo_afiliacion) {
        this.tipo_afiliacion = tipo_afiliacion;
    }

    public Afiliado(LocalDate fecha_afiliacion, LocalDate fecha_registro, EstadoAfiliado estado_afiliacion,
            String tipo_afiliacion) {
        this.fecha_afiliacion = fecha_afiliacion;
        this.fecha_registro = fecha_registro;
        this.estado_afiliacion = estado_afiliacion;
        this.tipo_afiliacion = tipo_afiliacion;
    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString() + "\nFecha de afiliacion: " + fecha_afiliacion + "\nFecha de registro: "
                + fecha_registro + "\nEstado de afiliacion: " + estado_afiliacion + "\nTipo afiliacion: "
                + tipo_afiliacion);
        return sb.toString();
    }
}