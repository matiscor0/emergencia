package emergencia;

import java.time.LocalDate;

public class ComprobantePago {
    private Integer nro_comprobante;
    private LocalDate fecha_comprobante;
    private String detalle_comprobante;

    public ComprobantePago(Integer nro_comprobante, LocalDate fecha_comprobante, String detalle_comprobante) {
        this.nro_comprobante = nro_comprobante;
        this.fecha_comprobante = fecha_comprobante;
        this.detalle_comprobante = detalle_comprobante;
    }

    public Integer getNro_comprobante() {
        return nro_comprobante;
    }
    public void setNro_comprobante(Integer nro_comprobante) {
        this.nro_comprobante = nro_comprobante;
    }
    public LocalDate getFecha_comprobante() {
        return fecha_comprobante;
    }
    public void setFecha_comprobante(LocalDate fecha_comprobante) {
        this.fecha_comprobante = fecha_comprobante;
    }
    public String getDetalle_comprobante() {
        return detalle_comprobante;
    }

    public void setDetalle_comprobante(String detalle_comprobante) {
        this.detalle_comprobante = detalle_comprobante;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Numero de comprobante: " + nro_comprobante +
        "\nFecha comprobante: " + fecha_comprobante +
        "\nDetalle del comprobante: " + detalle_comprobante);
        return sb.toString();
    }
}
