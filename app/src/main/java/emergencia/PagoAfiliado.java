package emergencia;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PagoAfiliado extends Pago {
    private BigDecimal tarifa_afiliado;
    private BigDecimal tarifa_familiar;

    public PagoAfiliado(LocalDate fecha_pago, BigDecimal monto_pago, Integer nro_pago, MetodoPago metodo_pago,
            String detalle_pago, BigDecimal tarifa_afiliado, BigDecimal tarifa_familiar) {
        super(fecha_pago, monto_pago, nro_pago, metodo_pago, detalle_pago);
        this.tarifa_afiliado = tarifa_afiliado;
        this.tarifa_familiar = tarifa_familiar;
    }

    public BigDecimal getTarifa_afiliado() {
        return tarifa_afiliado;
    }
    public void setTarifa_afiliado(BigDecimal tarifa_afiliado) {
        this.tarifa_afiliado = tarifa_afiliado;
    }
    public BigDecimal getTarifa_familiar() {
        return tarifa_familiar;
    }

    public void setTarifa_familiar(BigDecimal tarifa_familiar) {
        this.tarifa_familiar = tarifa_familiar;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString() + "\nTarifa afiliado: " + tarifa_afiliado + "\nTarifa familiar: " + tarifa_familiar);
        return sb.toString();
    }
}
