package emergencia;

import java.time.LocalDate;

import emergencia.Direccion.Provincia;

public class Persona {
    private String nombre_completo;
    private LocalDate fecha_nacimiento;
    private Integer dni;
    private Direccion direccion;
    private Integer nro_telefono;
    private String correo;

    public Persona() {
    }
    public Persona(String nombre_completo, LocalDate fecha_nacimiento,Integer dni, Provincia provincia, String localidad,String barrio, String calle, Integer nro_calle, Integer nro_telefono,
            String correo) {
        this.nombre_completo = nombre_completo;
        this.fecha_nacimiento = fecha_nacimiento;
        this.dni = dni;
        this.direccion = new Direccion(provincia, localidad, barrio, calle, nro_calle);
        this.nro_telefono = nro_telefono;
        this.correo = correo;
    }

    public String getNombre_completo() {
        return nombre_completo;
    }
    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }
    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }
    public Integer getDni() {
        return dni;
    }
    public void setDni(Integer dni) {
        this.dni = dni;
    }
    public Direccion getDireccion() {
        return direccion;
    }
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    public Integer getNro_telefono() {
        return nro_telefono;
    }
    public void setNro_telefono(Integer nro_telefono) {
        this.nro_telefono = nro_telefono;
    }
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nombre completo: " + nombre_completo + "\nFecha de nacimiento: " + fecha_nacimiento + "\nD.N.I.: "
                + dni + direccion.toString() + "\nNumero de telefono: " + nro_telefono + "\nCorreo electronico: "
                + correo);
        return sb.toString();
    }
}
