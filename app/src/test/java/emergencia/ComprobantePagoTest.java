package emergencia;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class ComprobantePagoTest {
    @Test
    public void verificarComprobante() {
        LocalDate fecha_comprobante = LocalDate.of(2023,05,01);
        ComprobantePago comprobantePrueba = new ComprobantePago(1234, fecha_comprobante, "blabla");

        assertEquals(comprobantePrueba.toString(), "Numero de comprobante: 1234\nFecha comprobante: 2023-05-01\nDetalle del comprobante: blabla");
    }
}
