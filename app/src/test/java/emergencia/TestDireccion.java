package emergencia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import emergencia.Direccion.Provincia;

public class TestDireccion {
    @Test
    public void TesteoDireccion() {
        Direccion direc = new Direccion(Provincia.Catamarca, "Belen","San Antonio", "ab1", 123);
        assertEquals(direc.toString(), "\nProvincia: Catamarca\nLocalidad: Belen\nBarrio: San Antonio\nCalle: ab1\nNumero de calle: 123");
    }
}
