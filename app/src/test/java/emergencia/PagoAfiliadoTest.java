package emergencia;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;

import emergencia.Pago.MetodoPago;

public class PagoAfiliadoTest {
    @Test
    public void VerificarPagoAfiliado() {
        LocalDate fecha_pago = LocalDate.of(2023,05,01);
        PagoAfiliado pagoAfiliadoPrueba = new PagoAfiliado(fecha_pago, new BigDecimal(1000), 1234, MetodoPago.Efectivo,
                "blabla", new BigDecimal(500), new BigDecimal(200));
        
                assertEquals(pagoAfiliadoPrueba.toString(),
                "Fecha pago: 2023-05-01\nMonto pago: 1000\nNumero pago: 1234\nMetodo de pago: Efectivo\nDetalle del pago: blabla\nTarifa afiliado: 500\nTarifa familiar: 200");
    }
}
