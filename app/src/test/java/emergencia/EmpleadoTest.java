package emergencia;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class EmpleadoTest {
    @Test
        public void VerificarEmpleado(){
        Empleado empleado2 = new Empleado(
        Empleado.Puesto.Enfermero,LocalDate.of(1990, 1, 1),
        LocalTime.of(8, 0),
        LocalTime.of(14, 0),158000,"1234567890123456789012");

        assertEquals(empleado2.toString(), "---Empleado--"+
        "\nPuesto: Enfermero" +
        "\nHorario entrada: 08:00" +
        "\nHorario salida: 14:00" +
        "\nSueldo: 158000.0" +
        "\nCBU: 1234567890123456789012" +
        "\nFecha ingreso a la empresa: 1990-01-01" + 
        "\n------------"
        );
    }
}
