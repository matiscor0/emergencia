package emergencia;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;


public class TestAsistenciaMedica {
    @Test
    public void verificarAsistencia(){
        AsistenciaMedica asistencia = new AsistenciaMedica(AsistenciaMedica.TipoSolicitante.Afiliado,
        LocalDate.of(2023, 4, 20),AsistenciaMedica.Estado.Finalizado,
        ResAsisMedica.TipoAsistencia.Atencion,
        "Medicado");


        assertEquals(asistencia.toString(),"\n---Asistencia Medica---"+
        "\nTipo de solicitante: Afiliado"+
        "\nUltima fecha de pago: 2023-04-20"+
        "\nEstado: Finalizado"+
        "\n--- Resultado de Asistencia Medica ---"+
        "\nTipo de Asistencia: Atencion"+
        "\nDiagnostico medico: Medicado"+
        "\n------------"+
        "\n\nEl afiliado se encuentra dentro del plazo de mora.");
    }
}
