package emergencia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestPlanAfiliado {
    @Test
    public void verificarPlan() {
        PlanAfiliacion plan1 = new PlanAfiliacion("Basico", "Plan basico", 3000, "Cobertura basica");

        assertEquals(plan1.toString(), "Nombre del plan: Basico\nDescripcion del plan: Plan basico\nCosto del plan: 3000\nBeneficio plan: Cobertura basica");
    }
}
