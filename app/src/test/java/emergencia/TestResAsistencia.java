package emergencia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestResAsistencia {
    @Test
    public void verificarResultado(){
        ResAsisMedica resultado = new ResAsisMedica(ResAsisMedica.TipoAsistencia.Atencion,
        "Medicado");

        assertEquals(resultado.toString(),"\n--- Resultado de Asistencia Medica ---"+
        "\nTipo de Asistencia: Atencion"+
        "\nDiagnostico medico: Medicado"+
        "\n------------");

    }
}
