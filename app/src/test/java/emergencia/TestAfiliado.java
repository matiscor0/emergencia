package emergencia;


import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import emergencia.Afiliado.EstadoAfiliado;
import emergencia.Direccion.Provincia;


public class TestAfiliado {
    @Test
    public void verificacionAfiliado() {
        LocalDate fecha_nacimiento = LocalDate.of(2000, 05, 27);
        LocalDate fecha_afiliacion = LocalDate.of(2022, 05, 27);
        LocalDate fecha_registro = LocalDate.of(2023, 04, 27);

        Afiliado afiliadoPrueba = new Afiliado("Luciano", fecha_nacimiento, 123456, Provincia.Catamarca,
                "Fray Mamerto Esquiu", "San Antonio", "S/N", 1234, 12345, "qwer", fecha_afiliacion, fecha_registro,
                EstadoAfiliado.Activo, "comun");

                assertEquals(afiliadoPrueba.toString(), "Nombre completo: Luciano\nFecha de nacimiento: 2000-05-27\nD.N.I.: 123456\nProvincia: Catamarca\nLocalidad: Fray Mamerto Esquiu\nBarrio: San Antonio\nCalle: S/N\nNumero de calle: 1234\nNumero de telefono: 12345\nCorreo electronico: qwer\nFecha de afiliacion: 2022-05-27\nFecha de registro: 2023-04-27\nEstado de afiliacion: Activo\nTipo afiliacion: comun");
    } 
}
