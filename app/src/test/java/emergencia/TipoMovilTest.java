package emergencia;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class TipoMovilTest {
    @Test
    public void verificarTipoMovil(){
        TipoMovil t_movil = new TipoMovil(TipoMovil.Tipo.Administrativo,"Emergencias");
        
        assertEquals(t_movil.toString(),"\nTipo de vehiculo: Administrativo"+
        "\nDescripcion del vehiculo: Emergencias");
    }
}
