package emergencia;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;

import emergencia.Pago.MetodoPago;

public class PagoTest {
    @Test
    public void TesteoPago() {
        LocalDate fecha_pago = LocalDate.of(2023,05,01);
        Pago pagoPrueba = new Pago(fecha_pago, new BigDecimal(200), 124, MetodoPago.Transferencia, "blablabla");

        assertEquals(pagoPrueba.toString(),
                "Fecha pago: 2023-05-01\nMonto pago: 200\nNumero pago: 124\nMetodo de pago: Transferencia\nDetalle del pago: blablabla");
        
    }
}
