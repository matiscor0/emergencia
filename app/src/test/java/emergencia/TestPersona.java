package emergencia;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import emergencia.Direccion.Provincia;

public class TestPersona {
    @Test
    public void TesteoPersona() {
        LocalDate fecha_nacimiento = LocalDate.of(2000, 05, 27);
        Persona persoan1 = new Persona("Luciano",fecha_nacimiento, 12345, Provincia.Catamarca, "Santa Maria","San Pio X", "S/N", 111, 123, "qwer");
        
        assertEquals(persoan1.toString(), "Nombre completo: Luciano\nFecha de nacimiento: 2000-05-27\nD.N.I.: 12345\nProvincia: Catamarca\nLocalidad: Santa Maria\nBarrio: San Pio X\nCalle: S/N\nNumero de calle: 111\nNumero de telefono: 123\nCorreo electronico: qwer");
    }
}
