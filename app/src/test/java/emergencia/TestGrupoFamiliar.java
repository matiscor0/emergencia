package emergencia;

import java.time.LocalDate;

import org.junit.Test;

import emergencia.Direccion.Provincia;
import emergencia.GrupoFamiliar.EstadoRelacion;

import static org.junit.Assert.assertEquals;

public class TestGrupoFamiliar {
    @Test
    public void verificarGrupoFamiliar() {
        LocalDate fecha_nacimiento = LocalDate.of(2020, 9, 30);
        GrupoFamiliar familia = new GrupoFamiliar("Taner", fecha_nacimiento, 123456, Provincia.Catamarca,
                "Fray Mamerto Esquiu", "San Antonio", "S/N", 1234, 12345, "qwer", "hermano", EstadoRelacion.Espera);

        
        assertEquals(familia.toString(),"Nombre completo: Taner\nFecha de nacimiento: 2020-09-30\nD.N.I.: 123456\nProvincia: Catamarca\nLocalidad: Fray Mamerto Esquiu\nBarrio: San Antonio\nCalle: S/N\nNumero de calle: 1234\nNumero de telefono: 12345\nCorreo electronico: qwer\nRelacion con afiliado: hermano\nEstado de relacion: Espera");
    }
}
