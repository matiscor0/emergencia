package emergencia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class MovilTest {
    @Test
    public void verificarTipoMovil(){
        Movil movil1 = new Movil(TipoMovil.Tipo.Administrativo,"Tramites",Movil.Marca.Ford,
        2012,"YBM 692","Sanatorio Junin",Movil.Disponibilidad_movil.Disponible);

        assertEquals(movil1.toString(), "---Movil---"+
        "\nTipo de vehiculo: Administrativo"+
        "\nDescripcion del vehiculo: Tramites"+
        "\nMarca: Ford"+
        "\nModelo: 2012"+
        "\nPatente: YBM 692"+
        "\nUbicacion del movil: Sanatorio Junin"+
        "\nDisponibilidad del movil: Disponible"+
        "\n***Movil***\n");
    }
}
